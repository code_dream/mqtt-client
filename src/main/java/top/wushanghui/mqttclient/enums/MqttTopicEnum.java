package top.wushanghui.mqttclient.enums;

public enum MqttTopicEnum {

    /**
     * 默认topic
     * 使用了通配符
     */
    DEFAULT_TOPIC("sensor/+/#", 0);

    /**
     * 主题
     */
    private String topic;
    /**
     * 服务质量
     */
    private Integer qos;

    MqttTopicEnum(String topic, Integer qos) {
        this.topic = topic;
        this.qos = qos;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Integer getQos() {
        return qos;
    }

    public void setQos(Integer qos) {
        this.qos = qos;
    }
}
