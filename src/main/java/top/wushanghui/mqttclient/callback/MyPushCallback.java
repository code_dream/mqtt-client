package top.wushanghui.mqttclient.callback;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.stereotype.Component;
import top.wushanghui.mqttclient.enums.MqttTopicEnum;

/**
 * @author 吴尚慧
 * @since 2021/11/11 20:36
 */
@Slf4j
@Component
public class MyPushCallback implements MqttCallbackExtended {

    private MqttClient mqttClient;

    @Override
    public void connectComplete(boolean reconnect, String serverURI) {
        log.info("MyPushCallback connectComplete reconnect={}, serverURI={}", reconnect, serverURI);
        if (reconnect) {
            // 重新连接后，需重新订阅主题
            try {
                log.info("mqttClient={}", mqttClient);
                mqttClient.subscribe(MqttTopicEnum.DEFAULT_TOPIC.getTopic(), MqttTopicEnum.DEFAULT_TOPIC.getQos());
            } catch (MqttException e) {
                log.error("订阅主题失败", e);
            }
        }
    }

    @Override
    public void connectionLost(Throwable cause) {
        log.error("MyPushCallback connectionLost cause={}", cause.toString());
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        log.info("MyPushCallback messageArrived topic={}, message={}", topic, message);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        log.info("MyPushCallback deliveryComplete token={}", token);
    }

    public void setMqttClient(MqttClient mqttClient) {
        this.mqttClient = mqttClient;
    }
}
